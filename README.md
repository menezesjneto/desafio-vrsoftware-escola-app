# DESAFIO VR SOFTWARE

Executar em modo dev: `flutter run -t lib/main_dev.dart`

Fazer build em modo dev: `flutter build apk --release -t lib/main_dev.dart --no-sound-null-safety`

Fazer build em modo prod: `flutter build apk --release -t lib/main_prod.dart --no-sound-null-safety`


Gerar arquivos do mobx: `flutter packages pub run build_runner build`


Desenvolvido por José Maria Neto - Desenvolvedor Mobile


menezesneto13@hotmail.com

