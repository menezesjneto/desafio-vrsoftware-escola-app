import 'package:desafiovrsoftwareapp/controllers/alunos_controller.dart';
import 'package:desafiovrsoftwareapp/controllers/curso_aluno_controller.dart';
import 'package:desafiovrsoftwareapp/controllers/cursos_controller.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:desafiovrsoftwareapp/controllers/loading_controller.dart';
import 'package:desafiovrsoftwareapp/resources/app_config.dart';
import 'main.dart';
import 'services/navigation_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();

  GetIt getIt = GetIt.I;
  getIt.registerSingleton<LoadingController>((LoadingController()));
  getIt.registerSingleton<CursosController>((CursosController()));
  getIt.registerSingleton<AlunosController>((AlunosController()));
  getIt.registerSingleton<CursoAlunoController>((CursoAlunoController()));

  var configureApp = AppConfig(
    buildType: 'dev',
    child: materialApp,
    urlServer: 'desafio-vrsoftware-escola-app.herokuapp.com',
  );

  return runApp(configureApp);
}
