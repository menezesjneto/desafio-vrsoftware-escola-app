import 'dart:async';
import 'dart:convert';
import 'package:desafiovrsoftwareapp/models/curso_model.dart';
import 'package:desafiovrsoftwareapp/providers/api_provider.dart';

class CursosProvider {
  static final CursosProvider _singleton = new CursosProvider._internal();
  factory CursosProvider() {
    return _singleton;
  }
  CursosProvider._internal();

  static Future<dynamic> getCursos() async {
    List<CursoModel> cursos = [];
    try {
      Map<String, String> queryParams = {};

      var res = await ApiProvider.restGet('cursos', queryParams, true, 'json');

      if (res['statusCode'] == 200) {
        cursos = CursoModel.listFromJson(res['cursos']);

        return {
          'value': true,
          'cursos': cursos.reversed,
          'msgRetorno': ['msgRetorno']
        };
      }

      return {
        'value': false,
        'cursos': cursos,
        'msgRetorno': res['msgRetorno']
      };
    } catch (e) {
      print('CursosProvider.getCursos: ${e.toString()}');
      return {
        'value': null,
        'cursos': cursos,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> getCursoById(codigoCurso) async {
    CursoModel? curso;
    try {
      Map<String, String> queryParams = {'codigoCurso': '$codigoCurso'};

      var res =
          await ApiProvider.restGet('cursosById', queryParams, true, 'json');

      if (res['statusCode'] == 200) {
        curso = CursoModel.fromJson(res['cursos'][0]);

        return {
          'value': true,
          'curso': curso,
          'msgRetorno': ['msgRetorno']
        };
      }

      return {'value': false, 'curso': curso, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.getCursosById: ${e.toString()}');
      return {
        'value': null,
        'curso': curso,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> cadCurso(CursoModel curso) async {
    try {
      Map<String, String> queryParams = {
        'ementa': "'" + curso.ementa! + "'",
        'descricao': "'" + curso.descricao! + "'",
      };

      var res =
          await ApiProvider.restPost('cursos', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.cadCurso: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> updateCurso(CursoModel curso) async {
    try {
      Map<String, dynamic> queryParams = {
        'ementa': "'" + curso.ementa! + "'",
        'descricao': "'" + curso.descricao! + "'",
        'codigo': '${curso.codigo}',
      };

      var res = await ApiProvider.restPost(
          'cursosUpdate', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.cadCurso: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> deleteCurso(CursoModel curso) async {
    try {
      Map<String, dynamic> queryParams = {
        'codigo': '${curso.codigo}',
      };

      var res = await ApiProvider.restPost(
          'cursosDeleteById', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.cadCurso: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }
}
