import 'dart:async';
import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:desafiovrsoftwareapp/models/curso_aluno_model.dart';
import 'package:desafiovrsoftwareapp/models/curso_model.dart';
import 'package:desafiovrsoftwareapp/providers/alunos_provider.dart';
import 'package:desafiovrsoftwareapp/providers/api_provider.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_provider.dart';

class CursosAlunosProvider {
  static final CursosAlunosProvider _singleton =
      new CursosAlunosProvider._internal();
  factory CursosAlunosProvider() {
    return _singleton;
  }
  CursosAlunosProvider._internal();

  static Future<dynamic> getCursosAlunos() async {
    List<CursoAlunoModel> cursosAlunos = [];
    try {
      Map<String, String> queryParams = {};

      var res =
          await ApiProvider.restGet('cursosalunos', queryParams, true, 'json');

      if (res['statusCode'] == 200) {
        cursosAlunos = CursoAlunoModel.listFromJson(res['cursosAlunos']);
        for (var item in cursosAlunos) {
          item.aluno =
              (await AlunosProvider.getAlunoById(item.codigoAluno))['aluno'];
          item.curso =
              (await CursosProvider.getCursoById(item.codigoCurso))['curso'];
        }
        return {
          'value': true,
          'cursosAlunos': cursosAlunos,
          'msgRetorno': res['msgRetorno']
        };
      }

      return {
        'value': false,
        'cursosAlunos': cursosAlunos,
        'msgRetorno': res['msgRetorno']
      };
    } catch (e) {
      print('CursosAlunosProvider.getCursos: ${e.toString()}');
      return {
        'value': null,
        'cursosAlunos': cursosAlunos,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> getCursosAlunosByIdCurso(CursoModel curso) async {
    List<CursoAlunoModel> cursosAlunos = [];
    try {
      Map<String, dynamic> queryParams = {
        'codigoCurso': '${curso.codigo}',
      };

      var res = await ApiProvider.restPost(
          'cursoalunobyidcurso', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        cursosAlunos = CursoAlunoModel.listFromJson(res['cursosAlunos']);
        return {
          'value': true,
          'cursosAlunos': cursosAlunos,
          'msgRetorno': res['msgRetorno']
        };
      }

      return {
        'value': false,
        'cursosAlunos': cursosAlunos,
        'msgRetorno': res['msgRetorno']
      };
    } catch (e) {
      print('CursosAlunosProvider.getCursos: ${e.toString()}');
      return {
        'value': null,
        'cursosAlunos': cursosAlunos,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> getCursosAlunosByIdAluno(AlunoModel aluno) async {
    List<CursoAlunoModel> cursosAlunos = [];
    try {
      Map<String, dynamic> queryParams = {
        'codigoAluno': '${aluno.codigo}',
      };

      var res = await ApiProvider.restPost(
          'cursoalunobyidaluno', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        cursosAlunos = CursoAlunoModel.listFromJson(res['cursosAlunos']);
        return {
          'value': true,
          'cursosAlunos': cursosAlunos,
          'msgRetorno': res['msgRetorno']
        };
      }

      return {
        'value': false,
        'cursosAlunos': cursosAlunos,
        'msgRetorno': res['msgRetorno']
      };
    } catch (e) {
      print('CursosAlunosProvider.getCursosAlunosByIdAluno: ${e.toString()}');
      return {
        'value': null,
        'cursosAlunos': cursosAlunos,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> cadMatricula(CursoAlunoModel cursoAluno) async {
    try {
      Map<String, String> queryParams = {
        'codigoAluno': '${cursoAluno.codigoAluno}',
        'codigoCurso': '${cursoAluno.codigoCurso}',
      };

      var res = await ApiProvider.restPost(
          'cursoaluno', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.cadCurso: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> deleteMatricula(CursoAlunoModel cursoAluno) async {
    try {
      Map<String, dynamic> queryParams = {
        'codigo': '${cursoAluno.codigo}',
      };

      var res = await ApiProvider.restPost(
          'cursoAlunoDeleteById', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('AlunosProvider.deleteAluno: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  // static Future<dynamic> iniciarAula(InfoData infoData, AulaModel aula) async {
  //   try {
  //     InstrutorModel? instrutor;

  //     Map filter1 = infoData.toJson();

  //     filter1['idUsuario'] = instrutor!.s01Cdid;
  //     filter1['dtSituacao'] = DateTime.now().toString();
  //     filter1['cdAula'] = aula.p01Cdid;
  //     filter1['procedure'] = 'usp_p_APPIniciarAulaPratica';

  //     Map<String, dynamic> log = {'log': filter1};

  //     var data = json.encode(log);

  //     Map<String, dynamic> queryParameters = {'concentrador': data};

  //     var res =
  //         await ApiProvider.restPost('', {}, queryParameters, 'json', null);

  //     if (res['retorno'][0]['cdRetorno'] >= 200) {
  //       return {'value': true, 'msgRetorno': res['retorno'][0]['msgRetorno']};
  //     }

  //     return {'value': false, 'msgRetorno': res['retorno'][0]['msgRetorno']};
  //   } catch (e) {
  //     print('CursosAlunosProvider.iniciarAula: ${e.toString()}');
  //     return {
  //       'value': null,
  //       'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
  //     };
  //   }
  // }

}
