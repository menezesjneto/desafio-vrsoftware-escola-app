import 'dart:async';
import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:desafiovrsoftwareapp/providers/api_provider.dart';

class AlunosProvider {
  static final AlunosProvider _singleton = new AlunosProvider._internal();
  factory AlunosProvider() {
    return _singleton;
  }
  AlunosProvider._internal();

  static Future<dynamic> getAlunos() async {
    List<AlunoModel> alunos = [];
    try {
      Map<String, String> queryParams = {};

      var res = await ApiProvider.restGet('alunos', queryParams, true, 'json');

      if (res['statusCode'] == 200) {
        alunos = AlunoModel.listFromJson(res['alunos']);

        return {
          'value': true,
          'alunos': alunos,
          'msgRetorno': ['msgRetorno']
        };
      }

      return {
        'value': false,
        'alunos': alunos,
        'msgRetorno': res['msgRetorno']
      };
    } catch (e) {
      print('AlunosProvider.getCursos: ${e.toString()}');
      return {
        'value': null,
        'alunos': alunos,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> getAlunoById(codigoAluno) async {
    AlunoModel? aluno;
    try {
      Map<String, String> queryParams = {'codigoAluno': '$codigoAluno'};

      var res =
          await ApiProvider.restGet('alunosById', queryParams, true, 'json');

      if (res['statusCode'] == 200) {
        aluno = AlunoModel.fromJson(res['alunos'][0]);

        return {
          'value': true,
          'aluno': aluno,
          'msgRetorno': ['msgRetorno']
        };
      }

      return {'value': false, 'aluno': aluno, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('AlunosProvider.getAlunosById: ${e.toString()}');
      return {
        'value': null,
        'aluno': aluno,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> cadAluno(AlunoModel aluno) async {
    try {
      Map<String, String> queryParams = {
        'nome': "'" + aluno.nome! + "'",
      };

      var res =
          await ApiProvider.restPost('alunos', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.cadCurso: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> updateAluno(AlunoModel aluno) async {
    try {
      Map<String, dynamic> queryParams = {
        'nome': "'" + aluno.nome! + "'",
        'codigo': '${aluno.codigo}',
      };

      var res = await ApiProvider.restPost(
          'alunosUpdate', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('CursosProvider.cadCurso: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static Future<dynamic> deleteAluno(AlunoModel aluno) async {
    try {
      Map<String, dynamic> queryParams = {
        'codigo': '${aluno.codigo}',
      };

      var res = await ApiProvider.restPost(
          'alunisDeleteById', null, queryParams, null, 'json');

      if (res['statusCode'] == 200) {
        return {'value': true, 'msgRetorno': res['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['msgRetorno']};
    } catch (e) {
      print('AlunosProvider.deleteAluno: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }
}
