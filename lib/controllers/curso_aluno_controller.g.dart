// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'curso_aluno_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CursoAlunoController on CursoAlunoControllerBase, Store {
  final _$cursosAlunosAtom =
      Atom(name: 'CursoAlunoControllerBase.cursosAlunos');

  @override
  ObservableList<CursoAlunoModel> get cursosAlunos {
    _$cursosAlunosAtom.reportRead();
    return super.cursosAlunos;
  }

  @override
  set cursosAlunos(ObservableList<CursoAlunoModel> value) {
    _$cursosAlunosAtom.reportWrite(value, super.cursosAlunos, () {
      super.cursosAlunos = value;
    });
  }

  final _$CursoAlunoControllerBaseActionController =
      ActionController(name: 'CursoAlunoControllerBase');

  @override
  void clearCursosAlunos() {
    final _$actionInfo = _$CursoAlunoControllerBaseActionController.startAction(
        name: 'CursoAlunoControllerBase.clearCursosAlunos');
    try {
      return super.clearCursosAlunos();
    } finally {
      _$CursoAlunoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addCursosAlunos(CursoAlunoModel value) {
    final _$actionInfo = _$CursoAlunoControllerBaseActionController.startAction(
        name: 'CursoAlunoControllerBase.addCursosAlunos');
    try {
      return super.addCursosAlunos(value);
    } finally {
      _$CursoAlunoControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
cursosAlunos: ${cursosAlunos}
    ''';
  }
}
