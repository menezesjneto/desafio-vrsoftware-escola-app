// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alunos_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AlunosController on AlunosControllerBase, Store {
  final _$alunosAtom = Atom(name: 'AlunosControllerBase.alunos');

  @override
  ObservableList<AlunoModel> get alunos {
    _$alunosAtom.reportRead();
    return super.alunos;
  }

  @override
  set alunos(ObservableList<AlunoModel> value) {
    _$alunosAtom.reportWrite(value, super.alunos, () {
      super.alunos = value;
    });
  }

  final _$AlunosControllerBaseActionController =
      ActionController(name: 'AlunosControllerBase');

  @override
  void clearAlunos() {
    final _$actionInfo = _$AlunosControllerBaseActionController.startAction(
        name: 'AlunosControllerBase.clearAlunos');
    try {
      return super.clearAlunos();
    } finally {
      _$AlunosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addAluno(AlunoModel value) {
    final _$actionInfo = _$AlunosControllerBaseActionController.startAction(
        name: 'AlunosControllerBase.addAluno');
    try {
      return super.addAluno(value);
    } finally {
      _$AlunosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
alunos: ${alunos}
    ''';
  }
}
