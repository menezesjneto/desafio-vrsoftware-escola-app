import 'package:mobx/mobx.dart';
part 'loading_controller.g.dart';

class LoadingController = LoadingControllerBase with _$LoadingController;

abstract class LoadingControllerBase with Store {
  @observable
  bool? isLoad;

  @observable
  bool? isRefreshed;

  @action
  void setIsLoad(bool value) {
    isLoad = value;
  }

  @action
  void resetIsLoad() {
    isLoad = null;
  }

  @action
  void setIsRefreshed(bool value) {
    isRefreshed = value;
  }

  @action
  void resetIsRefreshed() {
    isRefreshed = null;
  }
}
