import 'package:desafiovrsoftwareapp/models/curso_aluno_model.dart';
import 'package:mobx/mobx.dart';
part 'curso_aluno_controller.g.dart';

class CursoAlunoController = CursoAlunoControllerBase
    with _$CursoAlunoController;

abstract class CursoAlunoControllerBase with Store {
  @observable
  ObservableList<CursoAlunoModel> cursosAlunos = ObservableList();

  @action
  void clearCursosAlunos() {
    cursosAlunos.clear();
  }

  @action
  void addCursosAlunos(CursoAlunoModel value) {
    cursosAlunos.add(value);
  }
}
