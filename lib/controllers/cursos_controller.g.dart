// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cursos_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CursosController on CursosControllerBase, Store {
  final _$cursosAtom = Atom(name: 'CursosControllerBase.cursos');

  @override
  ObservableList<CursoModel> get cursos {
    _$cursosAtom.reportRead();
    return super.cursos;
  }

  @override
  set cursos(ObservableList<CursoModel> value) {
    _$cursosAtom.reportWrite(value, super.cursos, () {
      super.cursos = value;
    });
  }

  final _$CursosControllerBaseActionController =
      ActionController(name: 'CursosControllerBase');

  @override
  void clearCursos() {
    final _$actionInfo = _$CursosControllerBaseActionController.startAction(
        name: 'CursosControllerBase.clearCursos');
    try {
      return super.clearCursos();
    } finally {
      _$CursosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addCurso(CursoModel value) {
    final _$actionInfo = _$CursosControllerBaseActionController.startAction(
        name: 'CursosControllerBase.addCurso');
    try {
      return super.addCurso(value);
    } finally {
      _$CursosControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
cursos: ${cursos}
    ''';
  }
}
