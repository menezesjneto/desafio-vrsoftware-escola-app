// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loading_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LoadingController on LoadingControllerBase, Store {
  final _$isLoadAtom = Atom(name: 'LoadingControllerBase.isLoad');

  @override
  bool? get isLoad {
    _$isLoadAtom.reportRead();
    return super.isLoad;
  }

  @override
  set isLoad(bool? value) {
    _$isLoadAtom.reportWrite(value, super.isLoad, () {
      super.isLoad = value;
    });
  }

  final _$isRefreshedAtom = Atom(name: 'LoadingControllerBase.isRefreshed');

  @override
  bool? get isRefreshed {
    _$isRefreshedAtom.reportRead();
    return super.isRefreshed;
  }

  @override
  set isRefreshed(bool? value) {
    _$isRefreshedAtom.reportWrite(value, super.isRefreshed, () {
      super.isRefreshed = value;
    });
  }

  final _$LoadingControllerBaseActionController =
      ActionController(name: 'LoadingControllerBase');

  @override
  void setIsLoad(bool value) {
    final _$actionInfo = _$LoadingControllerBaseActionController.startAction(
        name: 'LoadingControllerBase.setIsLoad');
    try {
      return super.setIsLoad(value);
    } finally {
      _$LoadingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetIsLoad() {
    final _$actionInfo = _$LoadingControllerBaseActionController.startAction(
        name: 'LoadingControllerBase.resetIsLoad');
    try {
      return super.resetIsLoad();
    } finally {
      _$LoadingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIsRefreshed(bool value) {
    final _$actionInfo = _$LoadingControllerBaseActionController.startAction(
        name: 'LoadingControllerBase.setIsRefreshed');
    try {
      return super.setIsRefreshed(value);
    } finally {
      _$LoadingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetIsRefreshed() {
    final _$actionInfo = _$LoadingControllerBaseActionController.startAction(
        name: 'LoadingControllerBase.resetIsRefreshed');
    try {
      return super.resetIsRefreshed();
    } finally {
      _$LoadingControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
isLoad: ${isLoad},
isRefreshed: ${isRefreshed}
    ''';
  }
}
