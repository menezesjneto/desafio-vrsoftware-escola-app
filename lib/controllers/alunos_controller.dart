import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:mobx/mobx.dart';
part 'alunos_controller.g.dart';

class AlunosController = AlunosControllerBase with _$AlunosController;

abstract class AlunosControllerBase with Store {
  @observable
  ObservableList<AlunoModel> alunos = ObservableList();

  @action
  void clearAlunos() {
    alunos.clear();
  }

  @action
  void addAluno(AlunoModel value) {
    alunos.add(value);
  }
}
