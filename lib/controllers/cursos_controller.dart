import 'package:desafiovrsoftwareapp/models/curso_model.dart';
import 'package:mobx/mobx.dart';
part 'cursos_controller.g.dart';

class CursosController = CursosControllerBase with _$CursosController;

abstract class CursosControllerBase with Store {
  @observable
  ObservableList<CursoModel> cursos = ObservableList();

  @action
  void clearCursos() {
    cursos.clear();
  }

  @action
  void addCurso(CursoModel value) {
    cursos.add(value);
  }
}
