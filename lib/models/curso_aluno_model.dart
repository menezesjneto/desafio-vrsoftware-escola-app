import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:desafiovrsoftwareapp/models/curso_model.dart';

class CursoAlunoModel {
  int? codigo;
  int? codigoAluno;
  int? codigoCurso;

  //---------- AUX
  AlunoModel? aluno;
  CursoModel? curso;

  CursoAlunoModel(
      {this.codigo,
      this.codigoAluno,
      this.codigoCurso,
      this.aluno,
      this.curso});

  factory CursoAlunoModel.fromJson(dynamic json) => CursoAlunoModel(
        codigo: json['codigo'],
        codigoAluno: json['codigo_aluno'],
        codigoCurso: json['codigo_curso'],
      );

  Map<String, dynamic> toMap() => {
        'codigo': codigo,
        'codigo_aluno': codigoAluno,
        'codigo_curso': codigoCurso,
      };

  static List<CursoAlunoModel> listFromJson(List<dynamic> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => CursoAlunoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CursoAlunoModel> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => post.toMap()).toList();
  }
}
