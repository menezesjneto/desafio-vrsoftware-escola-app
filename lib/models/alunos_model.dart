import 'package:desafiovrsoftwareapp/utils/extractor_json.dart';

class AlunoModel {
  int? codigo;
  String? nome;

  AlunoModel({
    this.codigo,
    this.nome,
  });

  factory AlunoModel.fromJson(dynamic json) => AlunoModel(
        codigo: json['codigo'],
        nome: Extractor.extractString(json['nome'], ''),
      );

  Map<String, dynamic> toMap() => {
        'codigo': codigo,
        'nome': nome,
      };

  static List<AlunoModel> listFromJson(List<dynamic> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => AlunoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<AlunoModel> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => post.toMap()).toList();
  }
}
