import 'package:desafiovrsoftwareapp/utils/extractor_json.dart';

class CursoModel {
  int? codigo;
  String? descricao;
  String? ementa;

  CursoModel({
    this.codigo,
    this.descricao,
    this.ementa,
  });

  factory CursoModel.fromJson(dynamic json) => CursoModel(
        codigo: json['codigo'],
        descricao: Extractor.extractString(json['descricao'], ''),
        ementa: Extractor.extractString(json['ementa'], ''),
      );

  Map<String, dynamic> toMap() => {
        'codigo': codigo,
        'descricao': descricao,
        'ementa': ementa,
      };

  static List<CursoModel> listFromJson(List<dynamic> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => CursoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CursoModel> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => post.toMap()).toList();
  }
}
