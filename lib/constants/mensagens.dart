const Mensagens = {
  'ERROR_LOGIN': {'titulo': 'Erro!', 'mensagem': 'Verifique se seus dados estão corretos.'},
  'SUCESSO_EDITAR_PERFIL': {'titulo': 'Perfil atualizado!', 'mensagem': '' },
  'ERRO_EDITAR_PERFIL': {'titulo': 'Erro!', 'mensagem': 'Perfil não atualizado! Verifique seus dados e sua conexão!'},
  'ERRO_CADASTRO': {'titulo': 'Erro no cadastro!', 'mensagem': 'Verifique seus dados e conexão. Tente novamente!'},
  'EMAIL_ENVIADO': {'titulo': 'E-mail enviado!', 'mensagem': 'Faça a troca de senha quando receber o e-mail!'},
  'ERRO_ENVIO_EMAIL': {'titulo': 'E-mail não enviado!', 'mensagem': 'Verifique se o e-mail é válido!'},
  'EMAIL_ADMIN_ENVIADO': {'titulo': 'E-mail enviado!\nAguarde nossa resposta!', 'mensagem': ''},
  'EMAIL_ADMIN_NAO_ENVIADO': {'titulo': 'E-mail não enviado!', 'mensagem': 'Verifique seus dados e sua conexão!'},
  'STATUS_ATUALIZADO': {'titulo': 'Status enviado!', 'mensagem': ''},
  'STATUS_NAO_ATUALIZADO': {'titulo': 'Status não enviado!', 'mensagem': 'Verifique sua conexão!'},
  'DADOS_ATUALIZADOS': {'titulo': 'Dados atualizados!', 'mensagem': ''},
  'DADOS_NAO_ATUALIZADOS': {'titulo': 'Dados não atualizados!', 'mensagem': 'Verifique sua conexão!'},
  
  'ERRO_EDITAR_LOGIN': {'titulo': 'Login não atualizado!', 'mensagem': 'Verifique sua conexão ou tente outro login!'},
  'ERRO_EDITAR_SENHA': {'titulo': 'Senha não atualizada!', 'mensagem': 'Verifique sua conexão!'},
  'LOGIN_EXISTENTE': {'titulo': 'Login já utilizado. Inisira um login diferente!', 'mensagem': ''},
  'EMAIL_EXISTENTE': {'titulo': 'Email já utilizado. Inisira um email diferente!', 'mensagem': ''},

  'ORDEM_CRIADA': {'titulo': 'Ordem criada!', 'mensagem': ''},
  'ORDEM_NAO_CRIADA': {'titulo': 'Ordem não criada!', 'mensagem': ''},
  'ORDEM_ATUALIZADA': {'titulo': 'Ordem atualizada!', 'mensagem': ''},
  'ORDEM_NAO_ATUALIZADA': {'titulo': 'Ordem não atualizada!', 'mensagem': ''},
  'ORDENS_ENVIADAS': {'titulo': 'Ordens enviadas!', 'mensagem': ''},
  'ORDENS_NAO_ENVIADAS': {'titulo': 'Ordens não enviadas!', 'mensagem': ''},

  'ANEXO_NAO_ENCONTRADO': {'titulo': 'Anexo não encontrado!', 'mensagem': ''},
};
