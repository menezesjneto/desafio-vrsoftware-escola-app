class AppException implements Exception {
  final _message;

  AppException([this._message]);

  String toString() {
    return "$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([message]) : super(message);
}

class BadRequestException extends AppException {
  BadRequestException([message]) : super(message);
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message]) : super(message);
}

class InvalidInputException extends AppException {
  InvalidInputException([message]) : super(message);
}

class NotFoundException extends AppException {
  NotFoundException([message]) : super(message);
}

class ForbbidenException extends AppException {
  ForbbidenException([message]) : super(message);
}

class InternalServerError extends AppException {
  InternalServerError([message]) : super(message);
}

class DataBaseException extends AppException {
  DataBaseException([message]) : super(message);
}