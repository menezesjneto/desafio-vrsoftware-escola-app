import 'package:flutter/material.dart';

class CustomsColors {

  static final MaterialColor customOrange = const MaterialColor(
    0xFFffcd05,
    const <int, Color>{
      50: const Color(0xFFffcd05),
      100: const Color(0xFFffcd05),
      200: const Color(0xFFffcd05),
      300: const Color(0xFFffcd05),
      400: const Color(0xFFffcd05),
      500: const Color(0xFFffcd05),
      600: const Color(0xFFffcd05),
      700: const Color(0xFFffcd05),
      800: const Color(0xFFffcd05),
      900: const Color(0xFFffcd05),
    },
  );

  static final MaterialColor customWhite = const MaterialColor(
    0xFFFFFFFF,
    const <int, Color>{
      50: const Color(0xFFFFFFFF),
      100: const Color(0xFFFFFFFF),
      200: const Color(0xFFFFFFFF),
      300: const Color(0xFFFFFFFF),
      400: const Color(0xFFFFFFFF),
      500: const Color(0xFFFFFFFF),
      600: const Color(0xFFFFFFFF),
      700: const Color(0xFFFFFFFF),
      800: const Color(0xFFFFFFFF),
      900: const Color(0xFFFFFFFF),
    },
  );

  static final MaterialColor customBlack = const MaterialColor(
    0xFF000000,
    const <int, Color>{
      50: const Color(0xFF000000),
      100: const Color(0xFF000000),
      200: const Color(0xFF000000),
      300: const Color(0xFF000000),
      400: const Color(0xFF000000),
      500: const Color(0xFF000000),
      600: const Color(0xFF000000),
      700: const Color(0xFF000000),
      800: const Color(0xFF000000),
      900: const Color(0xFF000000),
    },
  );

  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static String toHex(Color color) {
    return ('#'+
  //    '${color.alpha.toRadixString(16).padLeft(2, '0')}'+
      '${color.red.toRadixString(16).padLeft(2, '0')}'+
      '${color.green.toRadixString(16).padLeft(2, '0')}'+
      '${color.blue.toRadixString(16).padLeft(2, '0')}').toUpperCase();
  }
}
