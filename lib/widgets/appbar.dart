import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';

AppBar getAppBar(BuildContext context, String title,
    {List<Widget>? actions,
    bool? isSearch,
    PreferredSize? bottom,
    bool? whiteBackground,
    double? elevation,
    bool? isBlockReturn}) {
  return AppBar(
    elevation: elevation != null ? elevation : 1,
    iconTheme: IconThemeData(
        color: whiteBackground == true
            ? CustomsColors.customOrange
            : Colors.black),
    leading: IconButton(
      icon: Icon(isSearch == true ? Icons.close : Icons.keyboard_arrow_left,
          size: 40.0),
      onPressed: isBlockReturn == true
          ? null
          : () {
              Navigator.pop(context, null);
            },
    ),
    centerTitle: true,
    title: Text(title,
        style: TextStyle(
            color: whiteBackground == true
                ? CustomsColors.customOrange
                : Colors.black)),
    backgroundColor:
        whiteBackground == true ? Colors.white : CustomsColors.customOrange,
    actions: actions,
    bottom: bottom,
  );
}

// appbar that return to the old screen
AppBar appBarWithBack(BuildContext context, String title) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.white),
    leading: IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        Navigator.pop(context, null);
      },
    ),
    centerTitle: true,
    title: Text(title, style: TextStyle(color: Colors.white)),
    backgroundColor: CustomsColors.customOrange,
  );
}

AppBar getAppBarHome(BuildContext context, String title,
    {List<Widget>? actions, PreferredSize? bottom}) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.white),
    automaticallyImplyLeading: false,
    centerTitle: true,
    title: Text(title, style: TextStyle(color: Colors.black)),
    backgroundColor: CustomsColors.customOrange,
    actions: actions,
    bottom: bottom,
  );
}
