import 'package:desafiovrsoftwareapp/controllers/curso_aluno_controller.dart';
import 'package:desafiovrsoftwareapp/controllers/cursos_controller.dart';
import 'package:desafiovrsoftwareapp/controllers/loading_controller.dart';
import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:desafiovrsoftwareapp/models/curso_aluno_model.dart';
import 'package:desafiovrsoftwareapp/models/curso_model.dart';
import 'package:desafiovrsoftwareapp/providers/alunos_provider.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_alunos_provider.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_provider.dart';
import 'package:desafiovrsoftwareapp/utils/extractor_json.dart';
import 'package:desafiovrsoftwareapp/widgets/appbar.dart';
import 'package:desafiovrsoftwareapp/widgets/custom_input.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class CadCursoAlunoPage extends StatefulWidget {
  @override
  _CadCursoAlunoPageState createState() => new _CadCursoAlunoPageState();
}

class _CadCursoAlunoPageState extends State<CadCursoAlunoPage>
    with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController codigoAlunoController = new TextEditingController();
  TextEditingController codigoCursoController = new TextEditingController();

  bool sending = false;

  CursoAlunoModel cursoAlunoCad = CursoAlunoModel();

  final cursosAlunosCtrl = GetIt.I.get<CursoAlunoController>();
  List<CursoModel> cursos = [];
  List<AlunoModel> alunos = [];

  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _getCursos();
  }

  void _getCursos() {
    CursosProvider.getCursos().then((value) {
      setState(() {
        for (var item in value['cursos']) {
          cursos.add(item);
        }
        _getAlunos();
      });
    });
  }

  void _getAlunos() {
    AlunosProvider.getAlunos().then((value) {
      setState(() {
        for (var item in value['alunos']) {
          alunos.add(item);
        }
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: getAppBar(context, 'Realizar matrícula', actions: []),
      backgroundColor: Colors.grey[00],
      body: isLoading == true
          ? Container(
              margin: EdgeInsets.only(top: 80.0),
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      CustomsColors.customOrange)))
          : SafeArea(
              child: GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);

                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                  child: Container(
                      margin: EdgeInsets.only(
                          top: 0.0, left: 20.0, right: 20.0, bottom: 0.0),
                      alignment: Alignment.center,
                      child: Form(
                          key: _formKey,
                          child: CustomScrollView(slivers: <Widget>[
                            SliverList(
                                delegate: SliverChildListDelegate([
                              //Codigo do Aluno
                              CustomInput.getInputLabel('*Código do Aluno'),
                              Container(
                                  decoration: CustomInput.decorationCircular(),
                                  child: TextFormField(
                                    readOnly: true,
                                    controller: codigoAlunoController,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 18.0),
                                    decoration: CustomInput.inputDecorationI(
                                        'Código do Aluno',
                                        showError: true),
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    textInputAction: TextInputAction.next,
                                    cursorColor: Colors.black,
                                    minLines: 1,
                                    maxLines: 2,
                                    validator: (text) {
                                      if (text!.length < 4)
                                        return "Insira o código do aluno";
                                      return null;
                                    },
                                    onTap: () {
                                      selectAluno();
                                    },
                                  )),

                              //Codigo do Curso
                              CustomInput.getInputLabel('*Código do Curso'),
                              Container(
                                  decoration: CustomInput.decorationCircular(),
                                  child: TextFormField(
                                    readOnly: true,
                                    controller: codigoCursoController,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 18.0),
                                    decoration: CustomInput.inputDecorationI(
                                        'Código do Curso',
                                        showError: true),
                                    keyboardType: TextInputType.text,
                                    textCapitalization:
                                        TextCapitalization.words,
                                    textInputAction: TextInputAction.next,
                                    cursorColor: Colors.black,
                                    minLines: 1,
                                    maxLines: 2,
                                    validator: (text) {
                                      if (text!.length < 4)
                                        return "Insira o código do curso";
                                      return null;
                                    },
                                    onTap: () {
                                      selectCurso();
                                    },
                                  )),
                            ]))
                          ]))))),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        child: Container(
            margin: EdgeInsets.fromLTRB(20, 35, 20, 35),
            width: MediaQuery.of(context).size.width,
            color: Colors.transparent,
            child: new TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(CustomsColors.customOrange),
                overlayColor:
                    MaterialStateProperty.all(CustomsColors.customOrange),
              ),
              child: sending
                  ? CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.white))
                  : Text('CADASTRAR',
                      style: TextStyle(color: Colors.white, fontSize: 18.0)),
              onPressed: sending
                  ? null
                  : () async {
                      final form = _formKey.currentState;

                      if (form!.validate()) _criar();
                    },
            )),
      ),
    );
  }

  void _criar() {
    setState(() {
      sending = true;
    });

    cursoAlunoCad.codigoAluno =
        Extractor.extractInt(codigoAlunoController.text.replaceAll("#", ""));
    cursoAlunoCad.codigoCurso =
        Extractor.extractInt(codigoCursoController.text.replaceAll("#", ""));

    CursosAlunosProvider.cadMatricula(cursoAlunoCad).then((value) {
      setState(() {
        sending = false;
      });
      CustomInput.showSnackbarSuccessMessage(_scaffoldKey, value['msgRetorno']);
      if (value['value'])
        Future.delayed(Duration(milliseconds: 2200)).then((value) {
          Navigator.pop(context, 'reload');
        });
    });
  }

  selectCurso() {
    showCupertinoModalBottomSheet(
      expand: false,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
          child: CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
            leading: Container(),
            middle: Text('Selecione o curso',
                style: TextStyle(color: Colors.orange))),
        child: SafeArea(
            bottom: false,
            child: ListView.builder(
                shrinkWrap: true,
                controller: ModalScrollController.of(context),
                physics: ClampingScrollPhysics(),
                itemCount: cursos.length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        codigoCursoController.text =
                            "#" + cursos[i].codigo.toString();

                        Navigator.pop(context);
                      });
                    },
                    child: ListTile(
                      title: Text(cursos[i].codigo.toString(),
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20,
                              fontWeight: FontWeight.w500)),
                      subtitle: Text(cursos[i].descricao.toString(),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              fontWeight: FontWeight.w400)),
                    ),
                  );
                })),
      )),
    );
  }

  selectAluno() {
    showCupertinoModalBottomSheet(
      expand: false,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) => Material(
          child: CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
            leading: Container(),
            middle: Text('Selecione o aluno',
                style: TextStyle(color: Colors.orange))),
        child: SafeArea(
            bottom: false,
            child: ListView.builder(
                shrinkWrap: true,
                controller: ModalScrollController.of(context),
                physics: ClampingScrollPhysics(),
                itemCount: alunos.length,
                itemBuilder: (context, i) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        codigoAlunoController.text =
                            "#" + alunos[i].codigo.toString();

                        Navigator.pop(context);
                      });
                    },
                    child: ListTile(
                      title: Text(alunos[i].codigo.toString(),
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20,
                              fontWeight: FontWeight.w500)),
                      subtitle: Text(alunos[i].nome.toString(),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                              fontWeight: FontWeight.w400)),
                    ),
                  );
                })),
      )),
    );
  }
}
