import 'package:desafiovrsoftwareapp/controllers/curso_aluno_controller.dart';
import 'package:desafiovrsoftwareapp/controllers/loading_controller.dart';
import 'package:desafiovrsoftwareapp/models/curso_aluno_model.dart';
import 'package:desafiovrsoftwareapp/pages/cursos_alunos/cad_curso_aluno_page.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_alunos_provider.dart';
import 'package:desafiovrsoftwareapp/widgets/custom_input.dart';
import 'package:desafiovrsoftwareapp/widgets/custom_widgets.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get_it/get_it.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../widgets/appbar.dart';

class CursosAlunosPage extends StatefulWidget {
  @override
  _CursosAlunosPageState createState() => _CursosAlunosPageState();
}

class _CursosAlunosPageState extends State<CursosAlunosPage>
    with AutomaticKeepAliveClientMixin {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  RefreshController _refreshController = new RefreshController();

  final loadCtrl = GetIt.I.get<LoadingController>();
  final cursosAlunosCtrl = GetIt.I.get<CursoAlunoController>();

  @override
  void initState() {
    super.initState();
    cursosAlunosCtrl.clearCursosAlunos();
    loadCtrl.setIsLoad(false);
    loadCtrl.setIsRefreshed(false);
    _getCursosAlunos();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  bool get wantKeepAlive => true;

  void _onRefresh() {
    if (loadCtrl.isLoad!) {
      _cleanOrdens();
    } else
      _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.grey[100],
        appBar: getAppBar(context, "Matrículas", actions: []),
        floatingActionButton: cadCursoAluno(),
        body: Observer(builder: (context) {
          return loadCtrl.isLoad == false
              ? Container(
                  margin: EdgeInsets.only(top: 80.0),
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          CustomsColors.customOrange)))
              : SmartRefresher(
                  primary: false,
                  enablePullDown: true,
                  enablePullUp: true,
                  header: MaterialClassicHeader(
                    color: CustomsColors.customOrange,
                    backgroundColor: Colors.grey[200],
                  ),
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  footer: CustomWidgets.customFooterRefresh(false),
                  child: CustomScrollView(
                      primary: false,
                      shrinkWrap: false,
                      slivers: <Widget>[
                        SliverList(
                            delegate: SliverChildListDelegate([
                          cursosAlunosCtrl.cursosAlunos.isEmpty
                              ? CustomWidgets.showEmptyListII(context,
                                  "Sem matrículas cadastradas\nno momento")
                              : Container(),
                          createListAlunos(context),
                          Container(height: 70.0),
                        ]))
                      ]),
                );
        }));
  }

  Widget createListAlunos(context) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        padding: EdgeInsets.only(top: 5.0),
        itemCount: cursosAlunosCtrl.cursosAlunos.length,
        itemBuilder: (BuildContext context, int index) {
          CursoAlunoModel cursoAlunoModel =
              cursosAlunosCtrl.cursosAlunos[index];
          return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: Duration(milliseconds: 600),
              columnCount: 1,
              delay: Duration(milliseconds: 350),
              child: SlideAnimation(
                  horizontalOffset: 50.0,
                  child: FlipAnimation(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white, width: 1.5),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.white),
                      alignment: Alignment.center,
                      child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: ListTile(
                            trailing: IconButton(
                              icon: Icon(LineAwesomeIcons.trash,
                                  color: Colors.red),
                              onPressed: () {
                                _showDialogDeletar(context, cursoAlunoModel);
                              },
                            ),
                            title: Container(
                              child: Text(
                                  "Matrícula " +
                                      cursoAlunoModel.codigo.toString(),
                                  style: TextStyle(
                                      fontSize: 22,
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold)),
                            ),
                            subtitle: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Código do aluno: ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                        TextSpan(
                                            text: "#" +
                                                cursoAlunoModel.aluno!.codigo
                                                    .toString(),
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Aluno: ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                        TextSpan(
                                            text: cursoAlunoModel.aluno!.nome,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ),
                                Divider(
                                  thickness: 1,
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Código do curso: ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                        TextSpan(
                                            text: "#" +
                                                cursoAlunoModel.curso!.codigo
                                                    .toString(),
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Descrição: ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                        TextSpan(
                                            text: cursoAlunoModel
                                                .curso!.descricao,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Ementa: ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                        TextSpan(
                                            text: cursoAlunoModel.curso!.ementa,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ),
                  )));
        });
  }

  void _cleanOrdens() {
    loadCtrl.setIsLoad(false);
    loadCtrl.setIsRefreshed(true);

    cursosAlunosCtrl.cursosAlunos.clear();
    _getCursosAlunos();
  }

  void _getCursosAlunos() {
    CursosAlunosProvider.getCursosAlunos().then((value) {
      loadCtrl.setIsLoad(true);

      _refreshController.loadComplete();
      if (loadCtrl.isRefreshed!) {
        loadCtrl.setIsRefreshed(false);
        _refreshController.refreshCompleted();
      }

      for (var item in value['cursosAlunos']) {
        cursosAlunosCtrl.cursosAlunos.add(item);
      }
    });
  }

  Widget cadCursoAluno() {
    return FloatingActionButton.extended(
      onPressed: () async {
        var result = await Navigator.push(context,
            MaterialPageRoute(builder: (context) => CadCursoAlunoPage()));
        if (result == 'reload') {
          cursosAlunosCtrl.clearCursosAlunos();
          loadCtrl.setIsLoad(false);
          loadCtrl.setIsRefreshed(false);
          _getCursosAlunos();
        }
      },
      label: Text('Realizar matrícula'),
      icon: Icon(Icons.add),
      backgroundColor: CustomsColors.customOrange,
    );
  }

  void _deletar(aluno) {
    cursosAlunosCtrl.clearCursosAlunos();
    loadCtrl.setIsLoad(false);
    loadCtrl.setIsRefreshed(false);

    CursosAlunosProvider.deleteMatricula(aluno).then((value) {
      loadCtrl.setIsLoad(false);
      loadCtrl.setIsRefreshed(false);
      _getCursosAlunos();
      CustomInput.showSnackbarSuccessMessage(scaffoldKey, value['msgRetorno']);
    });
  }

  void _showDialogDeletar(context, cursoAluno) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text("Deseja remover este aluno deste curso?"),
          content: Text("Esta ação não poderá ser revertida!"),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "Não",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
            FlatButton(
              child: Text("Sim"),
              onPressed: () {
                _deletar(cursoAluno);

                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }
}
