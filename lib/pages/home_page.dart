import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:desafiovrsoftwareapp/pages/alunos/alunos_page.dart';
import 'package:desafiovrsoftwareapp/pages/cursos/cursos_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:desafiovrsoftwareapp/widgets/theme.dart';

import '../widgets/theme.dart';
import 'cursos_alunos/cursos_alunos_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Map<dynamic, dynamic>> pages = [
    {
      "nome": "Cursos",
      "page": CursosPage(),
      "icon": Icon(FontAwesomeIcons.accusoft, color: Colors.black, size: 30),
      "id": 0,
    },
    {
      "nome": "Alunos",
      "page": AlunosPage(),
      "icon":
          Icon(FontAwesomeIcons.userGraduate, color: Colors.black, size: 30),
      "id": 1,
    },
    {
      "nome": "Matrículas",
      "page": CursosAlunosPage(),
      "icon": Icon(FontAwesomeIcons.idCardAlt, color: Colors.black, size: 30),
      "id": 1,
    },
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
        key: _scaffoldKey,
        body: CustomScrollView(slivers: <Widget>[
          SliverList(
              delegate: SliverChildListDelegate([
            Stack(alignment: Alignment.topRight, children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 60.0),
                width: MediaQuery.of(context).size.width,
                height: 160.0,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                      CustomsColors.customOrange,
                      Colors.orange[400]!
                    ])),
              ),
              Container(
                  padding: EdgeInsets.only(top: 60.0),
                  width: MediaQuery.of(context).size.width,
                  child: Column(children: <Widget>[
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              AnimatedTextKit(
                                animatedTexts: [
                                  ColorizeAnimatedText(
                                    'Desafio VR Software',
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 22,
                                        color: Colors.black),
                                    textAlign: TextAlign.center,
                                    colors: [
                                      Colors.black,
                                      Colors.orange,
                                      Colors.black45,
                                    ],
                                  ),
                                ],
                                isRepeatingAnimation: true,
                                totalRepeatCount: 50,
                                onTap: () {},
                              ),
                              SizedBox(height: 10),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('José Maria S. M. Neto',
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold)),
                                  Text('Desenvolvedor Flutter',
                                      style: TextStyle(
                                          color: Colors.black87,
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ],
                          ),
                          Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                image: DecorationImage(
                                    image:
                                        AssetImage("assets/imgs/logo1.png"))),
                          ),
                        ],
                      ),
                    ),
                  ])),
            ]),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 20.0, right: 20.0),
              child: createGrid(context),
            ),
          ])),
        ]));
  }

  Widget createGrid(context) {
    return GridView.builder(
      scrollDirection: Axis.vertical,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 5.0,
          mainAxisSpacing: 5.0,
          childAspectRatio: 1.2),
      primary: false,
      shrinkWrap: true,
      itemCount: pages.length,
      itemBuilder: (context, index) {
        return AnimationConfiguration.staggeredGrid(
            position: index,
            duration: Duration(milliseconds: 500),
            columnCount: 3,
            delay: Duration(milliseconds: 300),
            child: SlideAnimation(
              horizontalOffset: 50.0,
              child: FlipAnimation(
                child: DecoratedBox(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                          CustomsColors.customOrange,
                          Colors.orange[400]!
                        ])),
                    child: TextButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                          Colors.transparent,
                        ),
                        overlayColor: MaterialStateProperty.all<Color>(
                          Colors.black45,
                        ),
                      ),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: pages[index]['icon'],
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Text(pages[index]['nome'],
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 18),
                                  textAlign: TextAlign.center),
                            ),
                          ],
                        ),
                      ),
                      onPressed: () {
                        if (pages[index]['page'] != null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => pages[index]['page']));
                        }
                      },
                    )),
              ),
            ));
      },
    );
  }
}
