import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:desafiovrsoftwareapp/widgets/appbar.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../widgets/appbar.dart';
import '../intro/intro_page.dart';

class ConfigPage extends StatefulWidget {
  @override
  _ConfigPageState createState() => new _ConfigPageState();
}

class _ConfigPageState extends State<ConfigPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Map<dynamic, dynamic>> _views = [
    {
      "title": "Política de Privacidade",
      "icon": FontAwesomeIcons.fileAlt,
      "page": null,
      'link': 'https://www.vrsoft.com.br'
    },
    {"title": "Suporte", "icon": FontAwesomeIcons.at, "page": '/contatos'},
    {"title": "Sair", "icon": FontAwesomeIcons.signOutAlt, "page": null},
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: getAppBarHome(context, "Mais"),
      backgroundColor: Colors.grey[100],
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
            delegate: SliverChildListDelegate([
          _createList(context, _views),
        ]))
      ]),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: Container(
          alignment: Alignment.bottomCenter,
          color: Colors.grey[100],
          height: 40,
          child: Text(
            'Desafio VR Software Mobile - José Neto',
            style: TextStyle(color: Colors.black45, fontSize: 14),
          ),
        ),
      ),
    );
  }

  Widget _createList(context, views) {
    return ListView.builder(
        primary: false,
        shrinkWrap: true,
        padding: EdgeInsets.all(0.0),
        itemCount: views.length,
        itemBuilder: (context, index) {
          return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: Duration(milliseconds: 300),
              columnCount: 1,
              delay: Duration(milliseconds: 70),
              child: SlideAnimation(
                  horizontalOffset: 50.0,
                  child: FlipAnimation(
                      child: Column(
                    children: <Widget>[
                      ListTile(
                        leading: Container(
                          height: 50,
                          width: 50,
                          child: Icon(views[index]["icon"],
                              size: 25.0, color: Colors.black),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: CustomsColors.customOrange),
                        ),
                        contentPadding: EdgeInsets.only(
                            bottom: 10.0, left: 20.0, right: 20.0, top: 20),
                        title: new Text(views[index]["title"],
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w400)),
                        onTap: () async {
                          if (views[index]["page"] != null) {
                            var result = await Navigator.of(context)
                                .pushNamed(views[index]["page"]);

                            setState(() {});
                          } else if (views[index]['link'] != null) {
                            // link
                            await canLaunch(views[index]['link'])
                                ? await launch(views[index]['link'])
                                : throw '';
                          } else
                            _showDialogSair(context);
                        },
                      ),
                      Divider(height: 1.0, color: Colors.black12)
                    ],
                  ))));
        });
  }

  void _showDialogSair(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Deseja sair do app?",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0)),
          content: Text(
              "Você pode recuperar seus dados cadastrados fazendo o login novamente!",
              style: TextStyle(fontSize: 20.0)),
          actions: <Widget>[
            FlatButton(
              child: Text("Não"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text(
                "Sim",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => new IntroPage()),
                );
              },
            ),
          ],
        );
      },
    );
  }
}
