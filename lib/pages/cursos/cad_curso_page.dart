import 'package:desafiovrsoftwareapp/models/curso_model.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_alunos_provider.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_provider.dart';
import 'package:desafiovrsoftwareapp/widgets/appbar.dart';
import 'package:desafiovrsoftwareapp/widgets/custom_input.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class CadCursoPage extends StatefulWidget {
  CursoModel? curso;
  CadCursoPage({Key? key, this.curso}) : super(key: key);

  @override
  _CadCursoPageState createState() => new _CadCursoPageState();
}

class _CadCursoPageState extends State<CadCursoPage>
    with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController descricaoController = new TextEditingController();
  TextEditingController ementaController = new TextEditingController();

  final FocusNode descricaoFocus = FocusNode();
  final FocusNode ementaFocus = FocusNode();

  bool sending = false;

  CursoModel cursoCad = CursoModel(
    descricao: '',
    ementa: '',
  );

  @override
  void initState() {
    super.initState();

    if (widget.curso != null) {
      cursoCad.descricao = widget.curso!.descricao;
      cursoCad.ementa = widget.curso!.ementa;
      cursoCad.codigo = widget.curso!.codigo;

      descricaoController.text = cursoCad.descricao!;
      ementaController.text = cursoCad.ementa!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: getAppBar(
          context, widget.curso == null ? 'Novo Curso' : 'Editar curso',
          actions: []),
      backgroundColor: Colors.grey[00],
      body: SafeArea(
          child: GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: Container(
                  margin: EdgeInsets.only(
                      top: 0.0, left: 20.0, right: 20.0, bottom: 0.0),
                  alignment: Alignment.center,
                  child: Form(
                      key: _formKey,
                      child: CustomScrollView(slivers: <Widget>[
                        SliverList(
                            delegate: SliverChildListDelegate([
                          //Descrição
                          CustomInput.getInputLabel('*Descrição'),
                          Container(
                              decoration: CustomInput.decorationCircular(),
                              child: TextFormField(
                                controller: descricaoController,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18.0),
                                decoration: CustomInput.inputDecorationI(
                                    'Descrição',
                                    showError: true),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                cursorColor: Colors.black,
                                focusNode: descricaoFocus,
                                minLines: 1,
                                maxLines: 2,
                                validator: (text) {
                                  if (text!.length < 4)
                                    return "Insira a descrição";
                                  return null;
                                },
                                onFieldSubmitted: (term) {
                                  descricaoFocus.unfocus();
                                  FocusScope.of(context)
                                      .requestFocus(ementaFocus);
                                },
                                // onChanged: (val) => product.name = val),
                              )),
                          //Ementa
                          CustomInput.getInputLabel('*Ementa'),
                          Container(
                              decoration: CustomInput.decorationCircular(),
                              child: TextFormField(
                                controller: ementaController,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18.0),
                                decoration: CustomInput.inputDecorationI(
                                    'Ementa',
                                    showError: true),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                cursorColor: Colors.black,
                                focusNode: ementaFocus,
                                maxLines: 5,
                                validator: (text) {
                                  if (text!.length < 4)
                                    return "Insira a ementa";
                                  return null;
                                },
                                onFieldSubmitted: (term) {
                                  ementaFocus.unfocus();
                                  FocusScope.of(context)
                                      .requestFocus(ementaFocus);
                                },
                                // onChanged: (val) => product.name = val),
                              )),

                          widget.curso != null && sending == false
                              ? Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(top: 20),
                                  child: TextButton(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(LineAwesomeIcons.trash,
                                            color: Colors.red),
                                        Text(
                                          'Apagar curso',
                                          style: TextStyle(color: Colors.red),
                                        )
                                      ],
                                    ),
                                    onPressed: () {
                                      _showDialogDeletar(context, widget.curso);
                                    },
                                  ))
                              : Container(),
                        ]))
                      ]))))),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        child: Container(
            margin: EdgeInsets.fromLTRB(20, 35, 20, 35),
            width: MediaQuery.of(context).size.width,
            color: Colors.transparent,
            child: new TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(CustomsColors.customOrange),
                overlayColor:
                    MaterialStateProperty.all(CustomsColors.customOrange),
              ),
              child: sending
                  ? CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.white))
                  : Text(widget.curso == null ? 'CADASTRAR' : 'SALVAR',
                      style: TextStyle(color: Colors.white, fontSize: 18.0)),
              onPressed: sending
                  ? null
                  : () async {
                      final form = _formKey.currentState;

                      if (form!.validate()) {
                        if (widget.curso == null)
                          _criar();
                        else
                          _editar(widget.curso);
                      }
                    },
            )),
      ),
    );
  }

  void _criar() {
    setState(() {
      sending = true;
    });

    cursoCad.descricao = descricaoController.text;
    cursoCad.ementa = ementaController.text;

    CursosProvider.cadCurso(cursoCad).then((value) {
      setState(() {
        sending = false;
      });
      CustomInput.showSnackbarSuccessMessage(_scaffoldKey, value['msgRetorno']);
      if (value['value'])
        Future.delayed(Duration(milliseconds: 2200)).then((value) {
          Navigator.pop(context, 'reload');
        });
    });
  }

  void _editar(curso) {
    setState(() {
      sending = true;
    });

    cursoCad.descricao = descricaoController.text;
    cursoCad.ementa = ementaController.text;

    CursosProvider.updateCurso(cursoCad).then((value) {
      setState(() {
        sending = false;
      });
      CustomInput.showSnackbarSuccessMessage(_scaffoldKey, value['msgRetorno']);
      if (value['value'])
        Future.delayed(Duration(milliseconds: 2200)).then((value) {
          Navigator.pop(context, 'reload');
        });
    });
  }

  void _deletar(curso) {
    setState(() {
      sending = true;
    });

    CursosAlunosProvider.getCursosAlunosByIdCurso(curso).then((value) {
      if (value['cursosAlunos'].isEmpty) {
        CursosProvider.deleteCurso(curso).then((value2) {
          setState(() {
            sending = false;
          });
          CustomInput.showSnackbarSuccessMessage(
              _scaffoldKey, value2['msgRetorno']);
          if (value2['value'])
            Future.delayed(Duration(milliseconds: 2200)).then((value3) {
              Navigator.pop(context, 'reload');
            });
        });
      } else {
        setState(() {
          sending = false;
        });
        CustomInput.showSnackbarSuccessMessage(_scaffoldKey,
            'Este curso não pode ser apagado, pois existem alunos matriculados nele!');
      }
    });
  }

  void _showDialogDeletar(context, curso) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text("Deseja deletar este curso?"),
          content: Text("Esta ação não poderá ser revertida!"),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "Não",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
            FlatButton(
              child: Text("Sim"),
              onPressed: () {
                _deletar(curso);

                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }
}
