import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:desafiovrsoftwareapp/pages/config/config_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../widgets/theme.dart';
import 'home_page.dart';

class TabPage extends StatefulWidget {
  @override
  State createState() => new _TabPageState();
}

class _TabPageState extends State<TabPage> with TickerProviderStateMixin {
  late PageController pageController;
  int _selectedIndex = 0;

  List<Widget> pages = <Widget>[HomePage(), ConfigPage()];

  @override
  void initState() {
    super.initState();

    pageController = new PageController(
      keepPage: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Scaffold(
            body: PageView(
              controller: pageController,
              onPageChanged: (index) {
                setState(() {
                  _selectedIndex = index;
                });
              },
              children: pages,
              physics: NeverScrollableScrollPhysics(), // No sliding
            ),
            bottomNavigationBar: new Theme(
              data: Theme.of(context).copyWith(
                  canvasColor: Colors.grey[200],
                  textTheme: Theme.of(context).textTheme.copyWith(
                      caption:
                          new TextStyle(color: CustomsColors.customOrange))),
              child: BottomNavyBar(
                  iconSize: 30,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  backgroundColor: CustomsColors.customOrange,
                  selectedIndex: _selectedIndex,
                  showElevation: true, // use this to remove appBar's elevation
                  onItemSelected: (index) {
                    pageController.jumpToPage(index);
                  },
                  items: [
                    BottomNavyBarItem(
                      title: Text('   Menu'),
                      icon: Icon(FontAwesomeIcons.bars, color: Colors.black),
                      activeColor: Colors.black,
                      inactiveColor: Colors.white54,
                    ),
                    BottomNavyBarItem(
                      title: Text('   Mais'),
                      icon: Icon(FontAwesomeIcons.cog, color: Colors.black),
                      activeColor: Colors.black,
                      inactiveColor: Colors.white54,
                    ),
                  ]),
            )));
  }
}
