import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:desafiovrsoftwareapp/providers/alunos_provider.dart';
import 'package:desafiovrsoftwareapp/providers/cursos_alunos_provider.dart';
import 'package:desafiovrsoftwareapp/widgets/appbar.dart';
import 'package:desafiovrsoftwareapp/widgets/custom_input.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class CadAlunoPage extends StatefulWidget {
  AlunoModel? aluno;
  CadAlunoPage({Key? key, this.aluno}) : super(key: key);

  @override
  _CadAlunoPageState createState() => new _CadAlunoPageState();
}

class _CadAlunoPageState extends State<CadAlunoPage>
    with SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  TextEditingController nomeController = new TextEditingController();

  bool sending = false;

  AlunoModel alunoCad = AlunoModel(nome: '');

  @override
  void initState() {
    super.initState();

    if (widget.aluno != null) {
      alunoCad.nome = widget.aluno!.nome;
      alunoCad.codigo = widget.aluno!.codigo;

      nomeController.text = alunoCad.nome!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: getAppBar(
          context, widget.aluno == null ? 'Novo Aluno' : 'Editar aluno',
          actions: []),
      backgroundColor: Colors.grey[00],
      body: SafeArea(
          child: GestureDetector(
              onTap: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: Container(
                  margin: EdgeInsets.only(
                      top: 0.0, left: 20.0, right: 20.0, bottom: 0.0),
                  alignment: Alignment.center,
                  child: Form(
                      key: _formKey,
                      child: CustomScrollView(slivers: <Widget>[
                        SliverList(
                            delegate: SliverChildListDelegate([
                          //Nome
                          CustomInput.getInputLabel('*Nome'),
                          Container(
                              decoration: CustomInput.decorationCircular(),
                              child: TextFormField(
                                controller: nomeController,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 18.0),
                                decoration: CustomInput.inputDecorationI('Nome',
                                    showError: true),
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                cursorColor: Colors.black,
                                minLines: 1,
                                maxLines: 2,
                                validator: (text) {
                                  if (text!.length < 4)
                                    return "Insira o nome do aluno";
                                  return null;
                                },
                              )),

                          widget.aluno != null && sending == false
                              ? Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(top: 20),
                                  child: TextButton(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(LineAwesomeIcons.trash,
                                            color: Colors.red),
                                        Text(
                                          'Apagar aluno',
                                          style: TextStyle(color: Colors.red),
                                        )
                                      ],
                                    ),
                                    onPressed: () {
                                      _showDialogDeletar(context, widget.aluno);
                                    },
                                  ))
                              : Container(),
                        ]))
                      ]))))),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        width: MediaQuery.of(context).size.width,
        color: Colors.transparent,
        child: Container(
            margin: EdgeInsets.fromLTRB(20, 35, 20, 35),
            width: MediaQuery.of(context).size.width,
            color: Colors.transparent,
            child: new TextButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(CustomsColors.customOrange),
                overlayColor:
                    MaterialStateProperty.all(CustomsColors.customOrange),
              ),
              child: sending
                  ? CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.white))
                  : Text(widget.aluno == null ? 'CADASTRAR' : 'SALVAR',
                      style: TextStyle(color: Colors.white, fontSize: 18.0)),
              onPressed: sending
                  ? null
                  : () async {
                      final form = _formKey.currentState;

                      if (form!.validate()) {
                        if (widget.aluno == null)
                          _criar();
                        else
                          _editar(widget.aluno);
                      }
                    },
            )),
      ),
    );
  }

  void _criar() {
    setState(() {
      sending = true;
    });

    alunoCad.nome = nomeController.text;

    AlunosProvider.cadAluno(alunoCad).then((value) {
      setState(() {
        sending = false;
      });
      CustomInput.showSnackbarSuccessMessage(_scaffoldKey, value['msgRetorno']);
      if (value['value'])
        Future.delayed(Duration(milliseconds: 2200)).then((value) {
          Navigator.pop(context, 'reload');
        });
    });
  }

  void _editar(aluno) {
    setState(() {
      sending = true;
    });

    alunoCad.nome = nomeController.text;

    AlunosProvider.updateAluno(alunoCad).then((value) {
      setState(() {
        sending = false;
      });
      CustomInput.showSnackbarSuccessMessage(_scaffoldKey, value['msgRetorno']);
      if (value['value'])
        Future.delayed(Duration(milliseconds: 2200)).then((value) {
          Navigator.pop(context, 'reload');
        });
    });
  }

  void _deletar(aluno) {
    setState(() {
      sending = true;
    });

    CursosAlunosProvider.getCursosAlunosByIdAluno(aluno).then((value) {
      if (value['cursosAlunos'].isEmpty) {
        AlunosProvider.deleteAluno(aluno).then((value2) {
          setState(() {
            sending = false;
          });
          CustomInput.showSnackbarSuccessMessage(
              _scaffoldKey, value2['msgRetorno']);
          if (value2['value'])
            Future.delayed(Duration(milliseconds: 2200)).then((value3) {
              Navigator.pop(context, 'reload');
            });
        });
      } else {
        setState(() {
          sending = false;
        });
        CustomInput.showSnackbarSuccessMessage(_scaffoldKey,
            'Este aluno não pode ser apagado, pois ele está matriculado em um curso!');
      }
    });
  }

  void _showDialogDeletar(context, aluno) {
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text("Deseja deletar este aluno?"),
          content: Text("Esta ação não poderá ser revertida!"),
          actions: <Widget>[
            FlatButton(
              child: Text(
                "Não",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
              ),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
            FlatButton(
              child: Text("Sim"),
              onPressed: () {
                _deletar(aluno);

                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }
}
