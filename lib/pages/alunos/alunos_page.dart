import 'package:desafiovrsoftwareapp/controllers/alunos_controller.dart';
import 'package:desafiovrsoftwareapp/controllers/loading_controller.dart';
import 'package:desafiovrsoftwareapp/models/alunos_model.dart';
import 'package:desafiovrsoftwareapp/pages/alunos/cad_aluno_page.dart';
import 'package:desafiovrsoftwareapp/providers/alunos_provider.dart';
import 'package:desafiovrsoftwareapp/widgets/custom_widgets.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get_it/get_it.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../widgets/appbar.dart';

class AlunosPage extends StatefulWidget {
  @override
  _AlunosPageState createState() => _AlunosPageState();
}

class _AlunosPageState extends State<AlunosPage>
    with AutomaticKeepAliveClientMixin {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  RefreshController _refreshController = new RefreshController();

  final loadCtrl = GetIt.I.get<LoadingController>();
  final alunosCtrl = GetIt.I.get<AlunosController>();

  @override
  void initState() {
    super.initState();
    alunosCtrl.clearAlunos();
    loadCtrl.setIsLoad(false);
    loadCtrl.setIsRefreshed(false);
    _getAlunos();
    _refreshController = RefreshController(initialRefresh: false);
  }

  @override
  bool get wantKeepAlive => true;

  void _onRefresh() {
    if (loadCtrl.isLoad!) {
      _cleanOrdens();
    } else
      _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.grey[100],
        appBar: getAppBar(context, "Alunos", actions: []),
        floatingActionButton: cadAluno(),
        body: Observer(builder: (context) {
          return loadCtrl.isLoad == false
              ? Container(
                  margin: EdgeInsets.only(top: 80.0),
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          CustomsColors.customOrange)))
              : SmartRefresher(
                  primary: false,
                  enablePullDown: true,
                  enablePullUp: true,
                  header: MaterialClassicHeader(
                    color: CustomsColors.customOrange,
                    backgroundColor: Colors.grey[200],
                  ),
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  footer: CustomWidgets.customFooterRefresh(false),
                  child: CustomScrollView(
                      primary: false,
                      shrinkWrap: false,
                      slivers: <Widget>[
                        SliverList(
                            delegate: SliverChildListDelegate([
                          alunosCtrl.alunos.isEmpty
                              ? CustomWidgets.showEmptyListII(
                                  context, "Sem alunos cadastrados\nno momento")
                              : Container(),
                          createListAlunos(context),
                          Container(height: 70.0),
                        ]))
                      ]),
                );
        }));
  }

  Widget createListAlunos(context) {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        padding: EdgeInsets.only(top: 5.0),
        itemCount: alunosCtrl.alunos.length,
        itemBuilder: (BuildContext context, int index) {
          AlunoModel aluno = alunosCtrl.alunos[index];
          return AnimationConfiguration.staggeredGrid(
              position: index,
              duration: Duration(milliseconds: 600),
              columnCount: 1,
              delay: Duration(milliseconds: 350),
              child: SlideAnimation(
                  horizontalOffset: 50.0,
                  child: FlipAnimation(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white, width: 1.5),
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color: Colors.white),
                      alignment: Alignment.center,
                      child: Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: ListTile(
                            trailing: Icon(Icons.chevron_right),
                            title: Container(
                              child: Text("#" + aluno.codigo.toString(),
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold)),
                            ),
                            subtitle: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "Nome: ",
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600)),
                                        TextSpan(
                                            text: aluno.nome,
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.black87,
                                                fontWeight: FontWeight.w400)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            onTap: () async {
                              var result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          CadAlunoPage(aluno: aluno)));
                              if (result == 'reload') {
                                alunosCtrl.clearAlunos();
                                loadCtrl.setIsLoad(false);
                                loadCtrl.setIsRefreshed(false);
                                _getAlunos();
                              }
                            },
                          )),
                    ),
                  )));
        });
  }

  void _cleanOrdens() {
    loadCtrl.setIsLoad(false);
    loadCtrl.setIsRefreshed(true);

    alunosCtrl.alunos.clear();
    _getAlunos();
  }

  void _getAlunos() {
    AlunosProvider.getAlunos().then((value) {
      loadCtrl.setIsLoad(true);

      _refreshController.loadComplete();
      if (loadCtrl.isRefreshed!) {
        loadCtrl.setIsRefreshed(false);
        _refreshController.refreshCompleted();
      }

      for (var item in value['alunos']) {
        alunosCtrl.alunos.add(item);
      }
    });
  }

  Widget cadAluno() {
    return FloatingActionButton.extended(
      onPressed: () async {
        var result = await Navigator.push(
            context, MaterialPageRoute(builder: (context) => CadAlunoPage()));
        if (result == 'reload') {
          alunosCtrl.clearAlunos();
          loadCtrl.setIsLoad(false);
          loadCtrl.setIsRefreshed(false);
          _getAlunos();
        }
      },
      label: Text('Adicionar novo aluno'),
      icon: Icon(Icons.add),
      backgroundColor: CustomsColors.customOrange,
    );
  }
}
