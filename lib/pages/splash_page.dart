import 'dart:async';
import 'package:desafiovrsoftwareapp/pages/intro/intro_page.dart';
import 'package:flutter/material.dart';
import 'package:desafiovrsoftwareapp/resources/app_config.dart';

import '../providers/api_provider.dart';
import 'tab_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool isAnimatedImg = false;
  bool isAnimatedText = false;

  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        isAnimatedText = true;
      });

      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          isAnimatedImg = true;
        });

        Future.delayed(const Duration(milliseconds: 100), () async {
          checkIsLogin();
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    ApiProvider.setUrlServer(AppConfig.of(context)!.urlServer);
    ApiProvider.setBuildType(AppConfig.of(context)!.buildType);
    return Scaffold(
        body: Container(
            color: Colors.white,
            alignment: Alignment.center,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 500),
              transitionBuilder: (Widget child, Animation<double> animation) {
                return ScaleTransition(child: child, scale: animation);
              },
              child: ListView(
                primary: false,
                shrinkWrap: true,
                children: <Widget>[
                  Hero(
                      tag: 'splash',
                      child: Container(
                        height: MediaQuery.of(context).size.width / 2,
                        alignment: Alignment.center,
                        child: Image.asset(
                          "assets/imgs/logo1.png",
                          height: MediaQuery.of(context).size.width / 2,
                        ),
                      )),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 15),
                    child: AnimatedOpacity(
                        opacity: isAnimatedText && !isAnimatedImg ? 1.0 : 0.0,
                        duration: Duration(milliseconds: 500),
                        child: FutureBuilder(builder: (context, snapCustomer) {
                          return Text(
                            'Desafio VR Software\nJosé Maria',
                            style: TextStyle(
                                color: Colors.black87,
                                fontSize: 25.0,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          );
                        })),
                  )
                ],
              ),
            )));
  }

  Future<Null> checkIsLogin() async {
    Future.delayed(Duration(seconds: 1)).then((value) async {
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => IntroPage()),
      );
    });
  }
}
