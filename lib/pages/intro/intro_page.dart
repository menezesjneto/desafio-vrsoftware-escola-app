import 'package:desafiovrsoftwareapp/pages/home_page.dart';
import 'package:desafiovrsoftwareapp/pages/intro/sliding_widget.dart';
import 'package:desafiovrsoftwareapp/pages/tab_page.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IntroPage extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<IntroPage>
    with SingleTickerProviderStateMixin {
  AnimationController? _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1700),
    )..forward();
    super.initState();
  }

  @override
  void dispose() {
    _animationController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Positioned.fill(
              child: Container(
                height: height,
                width: width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        colors: [
                      CustomsColors.customOrange,
                      Colors.orange[400]!
                    ])),
              ),
            ),
            Positioned(
              top: height * 0.2,
              child: Column(
                children: <Widget>[
                  ScaleTransition(
                    scale: _animationController!.drive(
                      Tween<double>(begin: 0.3, end: 1.0).chain(
                        CurveTween(
                          curve: Interval(0.0, 0.2, curve: Curves.elasticInOut),
                        ),
                      ),
                    ),
                    child: FadeTransition(
                      opacity: _animationController!.drive(
                        Tween<double>(begin: 0.0, end: 1.0).chain(
                          CurveTween(
                            curve: Interval(0.2, 0.4, curve: Curves.decelerate),
                          ),
                        ),
                      ),
                      child: ScaleTransition(
                        scale: _animationController!.drive(
                          Tween<double>(begin: 1.3, end: 1.0).chain(
                            CurveTween(
                              curve: Interval(0.2, 0.4,
                                  curve: Curves.elasticInOut),
                            ),
                          ),
                        ),
                        child: FadingSlidingWidget(
                          animationController: _animationController,
                          interval: const Interval(0.2, 0.9),
                          child: Container(
                            width: width * 0.4,
                            height: width * 0.4,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: AssetImage(
                                      'assets/imgs/logo1.png',
                                    ))),
                          ),
                        ),
                      ),
                    ),
                  ),
                  FadingSlidingWidget(
                    animationController: _animationController,
                    interval: const Interval(0.5, 0.9),
                    child: Text(
                      'Escola',
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: width * 0.055,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.04,
                  ),
                  FadingSlidingWidget(
                    animationController: _animationController,
                    interval: const Interval(0.5, 0.9),
                    child: Text(
                      'Desafio VR Software',
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: width * 0.065,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.1,
                  ),
                  Container(
                    width: width * 0.8,
                    child: FadingSlidingWidget(
                      animationController: _animationController,
                      interval: const Interval(0.7, 1.0),
                      child: Text(
                        'José Maria S. M. Neto\nDesenvolvedor Flutter',
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: width * 0.050,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 20,
              child: GestureDetector(
                onTap: () async {
                  SharedPreferences pages =
                      await SharedPreferences.getInstance();
                  pages.setBool("introPageExibida", true);
                  //Navigator.push(context, SlideRightRoute(page: TabPage()));
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (BuildContext context) => TabPage()));
                },
                child: FadingSlidingWidget(
                  animationController: _animationController,
                  child: AnimatedContainer(
                    duration: const Duration(seconds: 1),
                    alignment: Alignment.center,
                    width: width * 0.8,
                    height: height * 0.075,
                    child: Text(
                      'Começar',
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: width * 0.05,
                          fontWeight: FontWeight.bold),
                    ),
                    decoration: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(width * 0.1),
                      ),
                      gradient: LinearGradient(colors: [
                        CustomsColors.customOrange,
                        Colors.orange,
                        Colors.orange[500]!,
                      ]),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ));
  }
}
