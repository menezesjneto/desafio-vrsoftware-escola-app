import 'package:desafiovrsoftwareapp/pages/not_found_page.dart';
import 'package:desafiovrsoftwareapp/routes/routes.dart';
import 'package:desafiovrsoftwareapp/widgets/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final materialApp = MaterialApp(
    title: "Desafio VRSoftware Mobile José Neto",
    theme: ThemeData(
        primarySwatch: CustomsColors.customOrange,
        primaryColor: Colors.white,
        fontFamily: 'Nunito'),
    debugShowCheckedModeBanner: false,
    localizationsDelegates: [
      GlobalCupertinoLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ],
    routes: routesApp,
    builder: (context, child) {
      return MediaQuery(
        child: child!,
        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      );
    });

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    default:
      return MaterialPageRoute(builder: (context) => NotFoundPage());
  }
}
