import 'package:desafiovrsoftwareapp/pages/splash_page.dart';
import 'package:flutter/material.dart';

import 'package:desafiovrsoftwareapp/pages/intro/intro_page.dart';
import 'package:desafiovrsoftwareapp/pages/not_found_page.dart';

import 'package:desafiovrsoftwareapp/pages/tab_page.dart';

import '../pages/config/config_page.dart';
import '../pages/config/contatos_page.dart';
import '../pages/config/politica_privacidade_page.dart';
import '../pages/config/termos_uso_page.dart';

import '../pages/home_page.dart';

final routesApp = {
  '/': (BuildContext context) => new SplashPage(),
  '/home': (BuildContext context) => new HomePage(),
  '/intro': (BuildContext context) => new IntroPage(),
  '/tab': (BuildContext context) => new TabPage(),
  '/config': (BuildContext context) => new ConfigPage(),
  '/politica_privacidade': (BuildContext context) =>
      new PoliticaPrivacidadePage(),
  '/termos_uso': (BuildContext context) => new TermosUsoPage(),
  '/contatos': (BuildContext context) => new ContatosPage(),
  '/erro': (BuildContext context) => new NotFoundPage(),
};
